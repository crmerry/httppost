#include "stdafx.h"
#include <Windows.h>
#include <strsafe.h>
#include <assert.h>
#include "SystemLog.h"
#include "HTTPPost.h"

using namespace serverlibrary;
HTTPPost::HTTPPost(const wchar_t * URL, const wchar_t * body, bool b_IP)
{
	Initialize(URL, body, b_IP);
}

HTTPPost::~HTTPPost()
{
	ReleaseBody();
}

void HTTPPost::ReInitialize(const wchar_t * URL, const wchar_t * body, bool b_IP)
{
	ReleaseBody();
	Initialize(URL, body, b_IP);
}

void HTTPPost::ChangeBody(const wchar_t * body)
{
	ReleaseBody();
	SetBody(body);
}

void HTTPPost::ChangePath(const wchar_t * path)
{
	wcscpy_s(_path, sizeof _path / sizeof(wchar_t), path);
}

bool HTTPPost::Connect()
{
	int error;
	_socket = socket(AF_INET, SOCK_STREAM, 0);

	if (_socket == INVALID_SOCKET)
	{
		int error = WSAGetLastError();

		systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not create HTTP socket.error code : %d", error);
		assert(0 && L"Can not create HTTP socket");

		return false;
	}

	/* ***************************************
	소켓 옵션 변경
	**************************************** */
	{
		LINGER linger;
		linger.l_onoff = 1;
		linger.l_linger = 0;

		error = setsockopt(_socket, SOL_SOCKET, SO_LINGER, (char*)&linger, sizeof linger);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change linger. error code : %d", error);
			assert(0 && L"Can not change linger");

			return false;
		}

		BOOL nagle_on_off = false;

		error = setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&nagle_on_off, sizeof nagle_on_off);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change nagle_on_off. error code : %d", error);
			assert(0 && L"Can not change nagle_on_off");

			return false;
		}

		DWORD send_timeout = 5000;

		error = setsockopt(_socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&send_timeout, sizeof send_timeout);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change send_timeout.error code : %d", error);
			assert(0 && L"Can not change send_timeout");

			return false;
		}

		DWORD recv_timeout = 5000;

		error = setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&recv_timeout, sizeof recv_timeout);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change recv_timeout.error code : %d", error);
			assert(0 && L"Can not change recv_timeout");

			return false;
		}

		u_long non_block_on = 1;
		error = ioctlsocket(_socket, FIONBIO, &non_block_on);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change non-block socket. error code : %d", error);
			assert(0 && L"Can not change non-block socket");

			return false;
		}
	}

	/* ***************************************
	연결
	**************************************** */
	error = connect(_socket, (SOCKADDR*)&_addr, sizeof _addr);

	if (error == SOCKET_ERROR)
	{
		error = WSAGetLastError();

		if (error != WSAEWOULDBLOCK)
		{
			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not connect. error code : %d", error);
			assert(0 && L"Can not connect");

			return false;
		}
	}

	fd_set w_set;
	timeval tval;

	FD_ZERO(&w_set);
	FD_SET(_socket, &w_set);

	tval.tv_sec = 2;
	tval.tv_usec = 0;

	/* ***************************************
	connect가 완료되면 활성화 된다.
	**************************************** */
	int result = select(1, nullptr, &w_set, nullptr, &tval);

	if (result == 0)
	{
		systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not connect timeout");
		assert(0 && L"Can not connect timeout");

		return false;
	}

	if (result == SOCKET_ERROR)
	{
		error = WSAGetLastError();

		systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not connect. error code : %d", error);
		assert(0 && L"Can not connect other reasons");

		return false;
	}

	/* ***************************************
	non block socket -> block socket
	**************************************** */
	u_long non_block_on = 0;
	error = ioctlsocket(_socket, FIONBIO, &non_block_on);

	if (error == SOCKET_ERROR)
	{
		error = WSAGetLastError();

		systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change non-block socket. error code : %d", error);
		assert(0 && L"Can not change non-block socket");

		return false;
	}

	return true;
}


bool HTTPPost::Post(int * out_response_code, wchar_t * out_response_body, int response_body_length)
{
	wchar_t HTTP_format[HEADER_LENGTH + BODY_LENGTH];

	/* ***************************************
	HTTP 포맷에 맞게 헤더 + 바디 작성
	**************************************** */
	swprintf_s(HTTP_format, _countof(HTTP_format),
		L"%s /%s %s\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\nConnection: Close\r\n\r\n%s",
		L"POST", _path, L"HTTP/1.1", _domain_or_IP, (int)wcslen(_body), _body);

	/* ***************************************
	conver utf-16 to utf-8
	**************************************** */
	char* utf8_text = nullptr;;
	UTF16toUTF8(&utf8_text, HTTP_format, (int)wcslen(HTTP_format));

	/* ***************************************
	block socket & send
	HTTP 통신이라 정확히 다 보내지 않으면, 응답도 없음...
	**************************************** */
	send(_socket, utf8_text, (int)strlen(utf8_text), 0);

	delete[] utf8_text;

	/* ***************************************
	block socket & recv
	HTTP의 헤더 크기가 정해져있지 않는 다는 점을 고려해야함.
	**************************************** */
	char* buf = new char[HEADER_LENGTH + BODY_LENGTH];
	buf[HEADER_LENGTH + BODY_LENGTH - 1] = '\0';
	int buf_index = 0;
	int body_length = 0;
	char junk_text[128]; // sscanf를 활용할 때, 필요 없는 텍스트를 거르기 위함.

	while (1)
	{
		int recv_size = recv(_socket, buf + buf_index, HEADER_LENGTH + BODY_LENGTH - buf_index, 0);

		if (recv_size == 0)
		{
			/* ***************************************
			끊어짐: 정상 종료
			**************************************** */
			*out_response_code = -1;

			return false;
		}
		else if (recv_size == SOCKET_ERROR)
		{
			/* ***************************************
			끊어짐: 소켓 에러
			**************************************** */
			*out_response_code = -1;

			int error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"HTTP connection disconnected. error code : %d ", error);
			assert(0 && L"HTTP connection disconnected");

			return false;
		}

		buf_index += recv_size;

		char* prev_body = strstr(buf, "\r\n\r\n");

		/* ***************************************
		\r\n\r\n을 찾아야(recv를 통해서 받아야) 헤더를 파악할 수 있다.
		**************************************** */
		if (prev_body == nullptr)
		{
			continue;
		}

		/* ***************************************
		위를 통과 했으므로, 헤더는 얻어낸 것.
		첫줄은 고정이므로, 바로 buf에 접근
		**************************************** */
		sscanf_s(buf, "%99[^ ] %99d", junk_text, (unsigned int)sizeof junk_text, out_response_code);

		/* ***************************************
		바디 길이
		**************************************** */
		char* content_length_ptr = strstr(buf, "Content-Length:");

		sscanf_s(content_length_ptr, "%99[^:]: %d", junk_text, (unsigned int)sizeof junk_text, &body_length);

		/* ***************************************
		\r\n\r\n까지 헤더 길이로 계산.
		**************************************** */
		int header_length = (int)((prev_body + 4) - buf);

		/* ***************************************
		헤더 길이 + 바디 길이 = 총 바이트.
		**************************************** */
		if (header_length + body_length == buf_index)
		{
			wchar_t* utf16_text = nullptr;
			UTF8toUTF16(&utf16_text, prev_body + 4, body_length);
			wcscpy_s(out_response_body, response_body_length, utf16_text);

			delete[] utf16_text;

			break;
		}
	}

	return true;
}

void HTTPPost::SetBody(const wchar_t * body)
{
	wcscpy_s(_body, BODY_LENGTH, body);
}

void HTTPPost::ReleaseBody()
{
	_body[0] = L'\0';
}

bool HTTPPost::Initialize(const wchar_t * URL, const wchar_t * body, bool b_IP)
{
	/* ***************************************
	http://www.abcd.com:5000/example.php
	_protocol : http
	_domain_or_IP : www.abcd.com
	_port : 5000
	_path : example.php
	**************************************** */
	swscanf_s(URL, L"%99[^:]://%99[^:]:%99d/%99[^\n]//$99", _protocol, (unsigned int)_countof(_protocol), _domain_or_IP, (unsigned int)_countof(_domain_or_IP), (int*)&_port, _path, (unsigned int)_countof(_path));

	ZeroMemory(&_addr, sizeof _addr);
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(_port);

	if (b_IP)
	{
		int error = InetPton(AF_INET, _domain_or_IP, &_addr.sin_addr);

		if (error == 0)
		{
			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Invalid IP");
			assert(0 && L"Invalid IP");

			return false;
		}
		else if (error == -1)
		{
			error = WSAGetLastError();

			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change IP string to binary. error code : %d", error);
			assert(0 && L"Can not change IP string to binary");

			return false;
		}
	}
	else
	{
		if (!DomainToIP(_domain_or_IP, &_addr.sin_addr))
		{
			systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not convert domain to IP");
			assert(0 && L"Can not convert domain to IP");

			return false;
		}
	}

	/* ***************************************
	body, paylaod, parameter... 어느 이름으로 불러도 되지만...
	**************************************** */
	SetBody(body);

	return true;
}

bool HTTPPost::DomainToIP(wchar_t * domain_name, IN_ADDR * address)
{
	ADDRINFOW*		address_info;

	int result = GetAddrInfo(domain_name, L"80", NULL, &address_info);

	if (result != 0)
	{
		int error = WSAGetLastError();

		systemlog::Log(L"HTTPPost", systemlog::eLogLevel::LOG_LEVEL_ERROR, L"Can not change domain to IP. error code : %d", error);
		assert(0 && L"Can not change domain to IP");

		return false;
	}

	*address = ((SOCKADDR_IN*)address_info->ai_addr)->sin_addr;

	/* ***************************************
	메모리 해제는 아래 매크로 함수로
	**************************************** */
	FreeAddrInfo(address_info);

	return true;
}

void HTTPPost::UTF16toUTF8(char ** utf8_text, const wchar_t * utf16_text, int utf16_len)
{
	int utf8_len = WideCharToMultiByte(CP_UTF8, 0, utf16_text, utf16_len, nullptr, 0, nullptr, nullptr);

	*utf8_text = new char[utf8_len + 1];
	(*utf8_text)[utf8_len] = '\0';

	WideCharToMultiByte(CP_UTF8, 0, utf16_text, utf16_len, *utf8_text, utf8_len, 0, 0);
}

void HTTPPost::UTF8toUTF16(wchar_t ** utf16_text, const char * utf8_text, int utf8_len)
{
	int utf16_len = MultiByteToWideChar(CP_UTF8, 0, utf8_text, utf8_len, nullptr, 0);

	*utf16_text = new wchar_t[utf16_len + 1];
	(*utf16_text)[utf16_len] = '\0';

	MultiByteToWideChar(CP_UTF8, 0, utf8_text, utf8_len, *utf16_text, utf16_len);
}