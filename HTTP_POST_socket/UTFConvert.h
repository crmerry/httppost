#pragma once

namespace serverlibrary
{
	void UTF16toUTF8(char** utf8_text, const wchar_t* utf16_text);
	void UTF16toUTF8(char* utf8_text, const wchar_t* utf16_text, int len);

	void UTF8toUTF16(wchar_t** utf16_text, const char* utf8_text);
	void UTF8toUTF16(wchar_t* utf16_text, const char* utf8_text, int len);
}