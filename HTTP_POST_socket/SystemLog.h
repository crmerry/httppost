#pragma once

namespace serverlibrary
{
	namespace systemlog
	{
		enum class eLogLevel
		{
			LOG_LEVEL_DEBUG = 0, 
			LOG_LEVEL_WARNG = 1,
			LOG_LEVEL_ERROR = 2,
			LOG_LEVEL_SYSTM = 3
		};

		enum class eStringLength
		{
			Dir = 128,
			FileName = 256,
			LogLevel = 32,
			LogFormat = 256,
			LogBody = 512,
		};
		
		wchar_t*	GetDir();
		eLogLevel	GetLogLevel();
		void		Initialize(const wchar_t* const target_dir, eLogLevel log_level);
		void		ChangeLogDir(const wchar_t* const dest_dir);
		void		ChangeLogLevel(eLogLevel log_level);
		void		Release();
		void		Log(const wchar_t* const type, eLogLevel log_level, const wchar_t* const log, ...);
		void		LogHex(const wchar_t* const type, eLogLevel log_level, const wchar_t* const log, BYTE* byte_arr, int byte_len);
		void		ByteToHexString(wchar_t** out_text, BYTE* byte_arr, int byte_len);
	}
}
