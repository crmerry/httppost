#pragma once

class HTTPPost
{
	enum
	{
		PROTOCOL_LENGTH = 16,
		PATH_LENGTH = 128,
		HOST_LENGTH = 128,
		HEADER_LENGTH = 256,
		BODY_LENGTH = 4096
	};

public:
	/* ***************************************
	url : http://www.test.com:5000/test.php , port 필수
	body : aaa=bbb&ccc=ddd... 또는 JSON
	b_IP : IP 또는 도메인
	**************************************** */
	HTTPPost(const wchar_t* URL, const wchar_t* body, bool b_IP);
	~HTTPPost();

	void ReInitialize(const wchar_t* URL, const wchar_t* body, bool b_IP);
	void ChangeBody(const wchar_t* body);
	void ChangePath(const wchar_t* path);
	
	bool Connect();

	/* ***************************************
	out_response_body의 크기가 BODY_LENTH(4096)가 크다면, BODY_LENGTH를 변경.
	**************************************** */
	bool Post(int* out_response_code, wchar_t* out_response_body, int response_body_length);

private:
	void SetBody(const wchar_t* body);
	void ReleaseBody();
	bool Initialize(const wchar_t* URL, const wchar_t* body, bool b_IP);
	bool DomainToIP(wchar_t* domain_name, IN_ADDR* address);
	void UTF16toUTF8(char** utf8_text, const wchar_t* utf16_text, int utf16_len);
	void UTF8toUTF16(wchar_t** utf16_text, const char* utf8_text, int utf8_len);

private:
	wchar_t		_protocol[PROTOCOL_LENGTH];
	wchar_t		_domain_or_IP[HOST_LENGTH];
	u_short		_port;
	wchar_t		_path[PATH_LENGTH];
	wchar_t		_body[BODY_LENGTH];

	SOCKET		_socket;
	SOCKADDR_IN _addr;
};
