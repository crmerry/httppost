// HTTP_POST.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "HTTPPost.h"
#include "Log.h"
#include "Profiler.h"

/* ***************************************
lib
**************************************** */
#pragma comment(lib, "Ws2_32.lib")

int main()
{
	InitializeLog();
	WSADATA wsa;

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		int error = WSAGetLastError();

		LOG(LOG_LEVEL_ERROR, L"Can not initialize WSAStartup. error code : %d", error);
		assert(0 && L"Can not initialize WSAStartup");

		return 0;
	}

	PROFILE_INITIALIZE;
	int response_code;
	wchar_t response_body[1024];
	int response_body_length = _countof(response_body);
	HTTPPost HTTP_post = HTTPPost(L"http://127.0.0.1:5000/auth_reg.php", L"id=yhggggdffgggdd&pass=dddd&nickname=dddd", false);
	HTTP_post.Connect();
	HTTP_post.Post(&response_code, response_body, response_body_length);

	HTTP_post.ChangePath(L"abcd.php");
	HTTP_post.Connect();
	HTTP_post.Post(&response_code, response_body, response_body_length);

	return 0;
}