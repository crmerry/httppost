#include "stdafx.h"
#include <Windows.h>
#include "UTFConvert.h"

namespace serverlibrary
{
	void UTF16toUTF8(char** utf8_text, const wchar_t* utf16_text)
	{
		int utf8_len = WideCharToMultiByte(CP_UTF8, 0, utf16_text, -1, nullptr, 0, 0, 0);

		*utf8_text = new char[utf8_len];

		WideCharToMultiByte(CP_UTF8, 0, utf16_text, -1, *utf8_text, utf8_len, 0, 0);
	}

	void UTF16toUTF8(char* utf8_text, const wchar_t* utf16_text, int len)
	{
		WideCharToMultiByte(CP_UTF8, 0, utf16_text, -1, utf8_text, len, 0, 0);
	}

	void UTF8toUTF16(wchar_t** utf16_text, const char* utf8_text)
	{
		int utf16_len = MultiByteToWideChar(CP_UTF8, 0, utf8_text, -1, nullptr, 0);

		*utf16_text = new wchar_t[utf16_len];

		MultiByteToWideChar(CP_UTF8, 0, utf8_text, -1, *utf16_text, utf16_len);
	}

	void UTF8toUTF16(wchar_t* utf16_text, const char* utf8_text, int len)
	{
		MultiByteToWideChar(CP_UTF8, 0, utf8_text, -1, utf16_text, len);
	}
}