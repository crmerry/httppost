#include "stdafx.h"
#include <Windows.h>
#include <strsafe.h>
#include <direct.h>
#include <time.h>
#include "UTFConvert.h"
#include "SystemLog.h"

namespace serverlibrary
{
	namespace systemlog
	{
		wchar_t		g_dir[128];
		eLogLevel	g_log_level;
		SRWLOCK		g_srw_lock;
		bool		gb_init;
		const wchar_t g_error_dir[] = L".\\error";
		const wchar_t g_hex_converter[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		wchar_t* GetDir()
		{
			return g_dir;
		}

		eLogLevel GetLogLevel()
		{
			return g_log_level;
		}

		/* ***************************************
		로그로 인한 문자열 생성으로 서버가 죽는 일은 막아야 함.
		그런데, Initialize는 딱 한 번 호출이므로, 
		여기서 죽으면 걍 죽게 냅두는게 나을 것 같은데
		**************************************** */
		void Initialize(const wchar_t* const dest_dir, eLogLevel log_level)
		{
			if (gb_init)
			{
				return;
			}

			char dir[static_cast<int>(eStringLength::Dir)];
			
			UTF16toUTF8(dir, dest_dir, sizeof dir);
			int result = _mkdir(dir);
			int error = errno;

			wcscpy_s(g_dir, _countof(g_dir), dest_dir);

			g_log_level = log_level;
			InitializeSRWLock(&g_srw_lock);
			gb_init = true;
		}

		void ChangeLogDir(const wchar_t* const dest_dir)
		{
			char dir[static_cast<int>(eStringLength::Dir)];

			UTF16toUTF8(dir, dest_dir, sizeof dir);
			_mkdir(dir);
			StringCchCopy(g_dir, _countof(g_dir), dest_dir);
		}

		void ChangeLogLevel(eLogLevel log_level)
		{
			g_log_level = log_level;
		}

		void Release()
		{
			/* ***************************************
			SRWLock은 Initialize는 있지만, 따로 자원을 해제하는 부분은 없음.
			이 외에 동적할당 없음.
			**************************************** */
		}

		/* ***************************************

		**************************************** */
		void Log(const wchar_t* const type, eLogLevel log_level, const wchar_t* const log, ...)
		{
			if (!(g_log_level <= log_level && gb_init))
			{
				return;
			}
			
			static __int64 log_counter;
			InterlockedIncrement64(&log_counter);

			wchar_t file_name[static_cast<int>(eStringLength::FileName)];
			wchar_t log_level_string[static_cast<int>(eStringLength::LogLevel)];
			wchar_t log_format[static_cast<int>(eStringLength::LogFormat)];
			wchar_t log_body[static_cast<int>(eStringLength::LogBody)];
			va_list v_list;
			struct tm t;
			time_t current;
			FILE* fp = nullptr;
			HRESULT result;

			time(&current);
			localtime_s(&t, &current);
			va_start(v_list, log);

			/* ***************************************
			file_name, log_format은 어느 정도 제한된 길이를 가지게 되지만,
			log_body 부분은 정해진 길이를 넘을 수 있음.
			**************************************** */
			result = StringCchVPrintf(log_body, _countof(log_body), log, v_list);

			va_end(v_list);

			if (result != S_OK)
			{
				/* ***************************************
				인자가 반영된 log_body가 너무 크므로 
				포맷만 복사하여 문제 해결 단서 남김.
				**************************************** */
				StringCchCopy(log_body, _countof(log_body), log);
				log_level = eLogLevel::LOG_LEVEL_ERROR;
			}

			/* ***************************************
			"dir""type"_"time".txt
			type이 너무 길어서 문제가 생길 경우, 문제 해결 단서를 위해
			짤린 파일 이름 그대로 감.
			(dir 길이가 문제라면 Initialize에서 문제 터짐)
			(time은 고정 길이)
			**************************************** */
			result = StringCchPrintf(file_name, _countof(file_name), L"%s\\%s_%d%02d.txt", g_dir, type, t.tm_year + 1900, t.tm_mon + 1);
			
			if (result != S_OK)
			{
				log_level = eLogLevel::LOG_LEVEL_ERROR;
			}

			switch (log_level)
			{
			case eLogLevel::LOG_LEVEL_DEBUG:
				wcscpy_s(log_level_string, 12, L"DEBUG");
				break;

			case eLogLevel::LOG_LEVEL_WARNG:
				wcscpy_s(log_level_string, 12, L"WARNG");
				break;

			case eLogLevel::LOG_LEVEL_ERROR:
				wcscpy_s(log_level_string, 12, L"ERROR");
				break;

			case eLogLevel::LOG_LEVEL_SYSTM:
				wcscpy_s(log_level_string, 12, L"SYSTM");
				break;

			default:
				wcscpy_s(log_level_string, 12, L"OUTRN");
				break;
			}
			
			
			/* ***************************************
			[Battle][2018-04-09 18:02:03 which one is better crt string function vs win32 api| DEBUG | 0000000001] 로그 내용 
			**************************************** */
			result = StringCchPrintf(log_format, _countof(log_format), L"[%8s][%d-%02d-%2d %02d:%02d:%02d | %s | %019I64d] ", type, t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, log_level_string, log_counter);

			/* ***************************************
			type을 제외한 나머지 포맷은 최대 길이가 정해져 있음.
			즉, type이 문제인 경우이므로 type에 대한 정보를 남김
			**************************************** */
			if (result != S_OK)
			{
				StringCchPrintf(log_format, _countof(log_format), L"[%s | ERROR ]", type);
			}
			
			AcquireSRWLockExclusive(&g_srw_lock);
			
			_wfopen_s(&fp, file_name, L"a");
			
			if (fp != nullptr)
			{
				fwprintf(fp, L"%s", log_format);
				fwprintf(fp, L"%s\n", log_body);
				fclose(fp);
			}
			
			ReleaseSRWLockExclusive(&g_srw_lock);
		}

		void LogHex(const wchar_t* const type, eLogLevel log_level, const wchar_t* const log, BYTE* byte_arr, int byte_len)
		{
			wchar_t* hex_string = nullptr;

			ByteToHexString(&hex_string, byte_arr, byte_len);
			Log(type, log_level, L"%s : 0x%s", log, hex_string);

			delete[] hex_string;
		}

		void ByteToHexString(wchar_t** out_text, BYTE* byte_arr, int byte_len)
		{
			wchar_t* after = new wchar_t[byte_len * 2 + 1];
			after[byte_len * 2] = L'\0';

			for (int index = 0; index < byte_len; index++)
			{
				after[index * 2] = g_hex_converter[(byte_arr[index] >> 4) & 0xF];
				after[index * 2 + 1] = g_hex_converter[(byte_arr[index] & 0xF)];
			}

			*out_text = after;
		}
	}
}